import sys, os
from glob import glob
from PyQt5.QtCore import pyqtSlot, QThread, QRunnable, QThreadPool
from PyQt5.QtWidgets import QMainWindow,QApplication, QTreeWidgetItem
from PyQt5.QtGui import QIcon
from ui import Ui_mainFrame
# Append scripts
sys.path.append("scripts")
from dicomProcessing import VisualWorker

### Main Window Class
class MainWindow(QMainWindow,Ui_mainFrame):
    """
    Main window User Interface
    """
    def __init__(self,*args, **kwargs):
        super(MainWindow,self).__init__(*args,**kwargs)
        self.setupUi(self)
        self.path = ""
        self.homePath = os.path.expanduser("~/")
        self.loadTree(self.homePath,self.treeView)
        self.currentElement = ""
        self.currentItem = None
        self.dicomList = []
        self.threadpool = QThreadPool()
        self.worker = VisualWorker()

        # Button configuration
        self.loadBtn.pressed.connect(self.loadBtnAction)
        self.resetBtn.pressed.connect(self.resetBtnAction)
        self.closeBtn.pressed.connect(self.closeBtnAction)

        # Tree Widget configuration
        self.treeView.itemDoubleClicked.connect(self.itemDoubleClicked)

    # Button functions
    def loadBtnAction(self):
        self.currentItem = self.treeView.currentItem()
        if self.currentItem is not None:
            loadPath = self.currentItem.statusTip(0)
            self.worker.dicomPath = loadPath
            for frame in ["axial","coronal","sagital"]:
                self.worker.readDicoms(frame)
            # self.threadpool.start(self.worker)

    def resetBtnAction(self):
        self.worker.stop()

    def closeBtnAction(self):
        self.worker.stop()
        app.quit()

    # Tree Directory actions
    def loadTree(self,path,tree):
        items = []
        for element in os.listdir(path):
            path_info = path + "/" + element
            if element[0] == ".":
                continue
            else:
                if os.path.isdir(path_info):
                    parent_itm = QTreeWidgetItem(tree, [os.path.basename(element)])
                    icon = QIcon("icons/openDirIcon.png")
                    icon.setThemeName("Directory")
                    parent_itm.setIcon(0,icon)
                    parent_itm.setStatusTip(0,path_info)
                else:
                    if ".dcm" in path_info:
                        parent_itm = QTreeWidgetItem(tree, [os.path.basename(element)])
                        icon = QIcon("icons/fileIcon.png")
                        icon.setThemeName("File")
                        parent_itm.setIcon(0,icon)
                        parent_itm.setStatusTip(0,path_info)
        tree.sortItems(0,0)

    def itemDoubleClicked(self,item):
        try:
            filename = item.statusTip(0)
            if not item.isExpanded() and "." not in item.text(0):
                self.loadTree(filename,item)
            else:
                pass
        except Exception as e:
            if 0:
                print("%s"%e)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    app.exec_()
