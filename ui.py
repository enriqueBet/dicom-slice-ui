# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'slices.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_mainFrame(object):
    def setupUi(self, mainFrame):
        mainFrame.setObjectName("mainFrame")
        mainFrame.resize(1364, 681)
        self.tabWidget = QtWidgets.QTabWidget(mainFrame)
        self.tabWidget.setGeometry(QtCore.QRect(460, 20, 891, 641))
        self.tabWidget.setObjectName("tabWidget")
        self.tab = QtWidgets.QWidget()
        self.tab.setObjectName("tab")
        self.sagitalFrame = QtWidgets.QFrame(self.tab)
        self.sagitalFrame.setGeometry(QtCore.QRect(0, 0, 881, 591))
        self.sagitalFrame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.sagitalFrame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.sagitalFrame.setObjectName("sagitalFrame")
        self.tabWidget.addTab(self.tab, "")
        self.tab_3 = QtWidgets.QWidget()
        self.tab_3.setObjectName("tab_3")
        self.axialFrame = QtWidgets.QFrame(self.tab_3)
        self.axialFrame.setGeometry(QtCore.QRect(0, 0, 881, 591))
        self.axialFrame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.axialFrame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.axialFrame.setObjectName("axialFrame")
        self.tabWidget.addTab(self.tab_3, "")
        self.tab_2 = QtWidgets.QWidget()
        self.tab_2.setObjectName("tab_2")
        self.coronalFrame = QtWidgets.QFrame(self.tab_2)
        self.coronalFrame.setGeometry(QtCore.QRect(0, 0, 881, 591))
        self.coronalFrame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.coronalFrame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.coronalFrame.setObjectName("coronalFrame")
        self.tabWidget.addTab(self.tab_2, "")
        self.loadBtn = QtWidgets.QPushButton(mainFrame)
        self.loadBtn.setGeometry(QtCore.QRect(98, 620, 104, 36))
        self.loadBtn.setObjectName("loadBtn")
        self.resetBtn = QtWidgets.QPushButton(mainFrame)
        self.resetBtn.setGeometry(QtCore.QRect(220, 620, 104, 36))
        self.resetBtn.setObjectName("resetBtn")
        self.closeBtn = QtWidgets.QPushButton(mainFrame)
        self.closeBtn.setGeometry(QtCore.QRect(340, 620, 104, 36))
        self.closeBtn.setObjectName("closeBtn")
        self.treeView = QtWidgets.QTreeWidget(mainFrame)
        self.treeView.setGeometry(QtCore.QRect(15, 20, 431, 591))
        self.treeView.setObjectName("treeView")
        self.actionDirectory = QtWidgets.QAction(mainFrame)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("icons/openDirIcon.png"), QtGui.QIcon.Normal, QtGui.QIcon.On)
        self.actionDirectory.setIcon(icon)
        self.actionDirectory.setObjectName("actionDirectory")

        self.retranslateUi(mainFrame)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(mainFrame)

    def retranslateUi(self, mainFrame):
        _translate = QtCore.QCoreApplication.translate
        mainFrame.setWindowTitle(_translate("mainFrame", "Slices"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), _translate("mainFrame", "Sagital"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_3), _translate("mainFrame", "Axial"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), _translate("mainFrame", "Coronal"))
        self.loadBtn.setText(_translate("mainFrame", "Load"))
        self.resetBtn.setText(_translate("mainFrame", "Reset"))
        self.closeBtn.setText(_translate("mainFrame", "Close"))
        self.treeView.headerItem().setText(0, _translate("mainFrame", "File Explorer"))
        self.actionDirectory.setText(_translate("mainFrame", "Directory"))
        self.actionDirectory.setShortcut(_translate("mainFrame", "Ctrl+O"))

