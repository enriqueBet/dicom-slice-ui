import sys, os, vtk, pydicom
from vtk.qt.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor
from PyQt5.QtCore import QRunnable, pyqtSlot, QRect
import __main__

### Visual Worker Class
class VisualWorker(QRunnable):
    """
    This class will take care to perform all image actions in a new thread
    """
    def __init__(self):
        super(VisualWorker,self).__init__()
        self.reader = ""
        self.dicomPath = ""
        self.vtkWidgets = []

    def readDicoms(self,frame):
        dicomPath = self.dicomPath
        self.reader = vtk.vtkDICOMImageReader()
        self.reader.SetDirectoryName(dicomPath)
        self.reader.Update()

        # Vtk image reconstruction features
        [xmin,xmax,ymin,ymax,zmin,zmax] = self.reader.GetExecutive().GetWholeExtent(self.reader.GetOutputInformation(0))
        self.spacing = self.reader.GetOutput().GetSpacing()
        self.origin = self.reader.GetOutput().GetOrigin()
        self.centralArray = (xmin + xmax, ymin + ymax, zmin + zmax)

        # For Odins sake
        o = self.origin
        s = self.spacing
        a = self.centralArray
        self.center = [o[0] + 0.5*s[0]*a[0],
                       o[1] + 0.5*s[1]*a[1],
                       o[2] + 0.5*s[2]*a[2]]
        c = self.center
        imageArray = vtk.vtkMatrix4x4()

        if "axial" in frame:
            self.vtkWidget = QVTKRenderWindowInteractor(parent=__main__.window.axialFrame)
            imageArray.DeepCopy((1,0,0,c[0],
                                 0,1,0,c[1],
                                 0,0,1,c[2],
                                 0,0,0,1))
        elif "coronal" in frame:
            self.vtkWidget = QVTKRenderWindowInteractor(parent=__main__.window.coronalFrame)
            imageArray.DeepCopy((1,0,0,c[0],
                                 0,0,1,c[1],
                                 0,-1,0,c[2],
                                 0,0,0,1))
        elif "sagital" in frame:
            self.vtkWidget = QVTKRenderWindowInteractor(parent=__main__.window.sagitalFrame)
            imageArray.DeepCopy((0,0,-1,c[0],
                                 0,1,0,c[1],
                                 -1,0,0,c[2],
                                 0,0,0,1))
        else:
            pass
        self.renderer = vtk.vtkRenderer()
        height = __main__.window.axialFrame.height()
        width = __main__.window.axialFrame.width()
        self.vtkWidget.setGeometry(QRect(0,0,width,height))
        self.vtkWidget.GetRenderWindow().AddRenderer(self.renderer)
        self.interactor = self.vtkWidget.GetRenderWindow().GetInteractor()

        # Reslicer
        self.reslice = vtk.vtkImageReslice()
        self.reslice.SetInputConnection(self.reader.GetOutputPort())
        self.reslice.SetOutputDimensionality(3)
        self.reslice.SetResliceAxes(imageArray)
        self.reslice.SetInterpolationModeToLinear()

        # Image Mapper
        self.imageMapper = vtk.vtkImageResliceMapper()
        self.imageMapper.SetInputConnection(self.reslice.GetOutputPort())
        self.imageMapper.SliceFacesCameraOn()
        self.imageMapper.SliceAtFocalPointOn()
        self.imageMapper.BorderOn()

        # Set Style
        self.style = vtk.vtkInteractorStyleImage()
        self.style.SetInteractionModeToImage3D()
        self.vtkWidget.SetInteractorStyle(self.style)

        # Properties
        self.imageProperty = vtk.vtkImageProperty()
        self.imageProperty.SetAmbient(0.0)
        self.imageProperty.SetDiffuse(1.0)
        self.imageProperty.SetOpacity(1.0)
        self.imageProperty.SetInterpolationTypeToLinear()

        # Image Actor
        self.imageActor = vtk.vtkImageSlice()
        self.imageActor.SetMapper(self.imageMapper)
        self.imageActor.SetProperty(self.imageProperty)

        # Add Everything to the frame
        self.renderer.AddViewProp(self.imageActor)
        self.renderer.SetBackground(255,255,255)

        # Initialize
        self.interactor.Initialize()
        self.vtkWidget.show()
        self.vtkWidgets.append(self.vtkWidget)

    @pyqtSlot()
    def run(self):
        pass

    @pyqtSlot()
    def stop(self):
        for widget in self.vtkWidgets:
            widget.close()
