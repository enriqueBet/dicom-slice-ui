# DICOM Slices Viewer

A simple DICOM slice viewer

## Overview

The goal of this project is to create a base application that will be useful to create image processing and image segmentation projects on the Medical Physics Field.

## Requirements
 - pydicom >= 1.3.0
 - PyQt5 >= 5.14.0
 - vtk >= 8.2.0

 If you are using Anaconda you can install the dependencies using the yaml file in the [misc/environment.yml](https://gitlab.com/enriqueBet/dicom-slice-ui/blob/master/misc/environment.yml):

## Key shortcuts

- **Zoom In/Out** | Move the mouse Up/Down while holding **Right Click**
- **Contrast +/-** |  Move the mouse Up/Down while holding **Left Click**
- **Saturation +/-** | Move the mouse Right/Left while holding **Right Click**
- **Image movement** | Move the mouse to any direction while holding the **Scroll Wheel Key** (Center Mouse key)
- **Rotate image** | Move the mouse to any direction while holding **CTRL + Right Click**
- **Rotate slice** | Move the mouse to any direction while holding **SHIFT + Right Click**
- **Change slice** | Move the mouse Up/Down while holding **SHIFT + Left Click**

## Screenshot
![](misc/screenshot.png)


## Acknowlegements

I would like to thank the projects enlisted above which without their awesome work this project wouldn't be possible:
- [VTK Project](https://vtk.org/) 
- [PyDicom](https://pydicom.github.io/)
- [Qt5](https://www.qt.io/)